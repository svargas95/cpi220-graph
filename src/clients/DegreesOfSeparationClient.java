package clients;

import datastructures.Bag;
import graph.BreadthFirstPaths;
import graph.CC;
import graph.DepthFirstPaths;
import graph.Graph;
import graph.SymbolGraph;
import io.StdIn;
import io.StdOut;

public class DegreesOfSeparationClient 
{
	public static void printAllAtDist(String source, SymbolGraph sg)
	{	int s = sg.index(source); // 
    	Graph G = sg.G();
    	BreadthFirstPaths bfs = new BreadthFirstPaths(G, s);
		 

	    while (!StdIn.isEmpty())
	    { 
	        int dist = new Integer(StdIn.readLine()).intValue();
			  System.out.println("Type in a distance: ");
	        BreadthFirstPaths b = new BreadthFirstPaths(G, s);
	       //System.out.printf("The distance between %d is %d\n ", s, b.distTo(dist));
	    }
	}
	
	public static void getNodesConnected(SymbolGraph sg, String source)
	{
		Graph G = sg.G();
		int s = sg.index(source);
		CC connectedComponents = new CC(G);
		
		//FILL IN REMAINDER OF METHOD HERE
	}
	
	public static void degreesOfSeparationDFS(String source, SymbolGraph sg)
	{
		int s = sg.index(source);
        Graph G = sg.G();
		DepthFirstPaths bfs = new DepthFirstPaths(G, s);
		
        System.out.println("Type in the name of a second node: ");

	    while (!StdIn.isEmpty()) 
	    {
	    	String sink = StdIn.readLine();
            if (sg.contains(sink)) {
                int t = sg.index(sink);
                if (bfs.hasPathTo(t)) {
                    for (int v : bfs.pathTo(t)) {
                        StdOut.println("   " + sg.name(v));
                    }
                }
                else {
                    StdOut.println("Not connected");
                }
            }
            else {
                StdOut.println("   Not in database.");
            }
        }
	}
	
	public static void degreesOfSeparationBFS(String source, SymbolGraph sg)
	{
		int s = sg.index(source);
        Graph G = sg.G();
		BreadthFirstPaths bfs = new BreadthFirstPaths(G, s);
		
		System.out.println("Type in the name of a second node:");
        while (!StdIn.isEmpty()) 
        {
            String sink = StdIn.readLine();
            if (sg.contains(sink)) {
                int t = sg.index(sink);
                if (bfs.hasPathTo(t)) {
                    for (int v : bfs.pathTo(t)) {
                        StdOut.println("   " + sg.name(v));
                    }
                }
                else {
                    StdOut.println("Not connected");
                }
            }
            else {
                StdOut.println("   Not in database.");
            }
        }	
	}
	
    public static void main(String[] args) 
    {
    	  //tring filename  = "src/data/movies.txt";
    	 String filename  = "movies.txt";
         String delimiter = "/";

         // StdOut.println("Source: " + source);
         SymbolGraph sg = new SymbolGraph(filename, delimiter);
         System.out.println("Type in the name of a node: ");

         while (StdIn.hasNextLine()) 
     	{
     		String source = StdIn.readLine();
     		if (!sg.contains(source)) {
    			StdOut.println(source + " not in database.");
    		}
     		else
     		{
     			//CALL METHODS HERE
     			DegreesOfSeparationClient.printAllAtDist(source, sg);
     		}
            System.out.println("Type in the name of a node: ");

     	}
    }
}
