# S L O W 
# INSTRUCTIONS
# make to compile
# cd bin/
# to run: java edu.asu.program.SorterApp
# make sure you modify all the source files to reference
# ../ instead of the current directory for files
# thanks java

all:
	find -name "*.java" > src.txt;
	cp src.txt cscope.files
	cscope -Rb
	javac -d bin @src.txt
	rm src.txt

clean: 
	find -name "*.class" > rm.txt;
	rm -I `cat rm.txt`
	rm rm.txt;
